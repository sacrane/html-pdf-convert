package com.sachin;

import java.io.FileOutputStream;
import java.io.IOException;

import com.realobjects.pdfreactor.webservice.client.Configuration;
import com.realobjects.pdfreactor.webservice.client.Configuration.Resource;
import com.realobjects.pdfreactor.webservice.client.PDFreactor;
import com.realobjects.pdfreactor.webservice.client.PDFreactorWebserviceException;

import com.realobjects.pdfreactor.webservice.client.Result;



public class HtmlToPdfConverter {
	
	public static void main(String[] args) throws IOException {
		
		// Create new PDFreactor instance
        PDFreactor pdfReactor = new PDFreactor();
        
     // Create a new configuration object
        Configuration config = new Configuration();
        config.setLicenseKey("The PDFreactor Software License Agreement and additional notices and license agreements for third-party software and/or open source software can be found in the\n" + 
        		"\n" + 
        		"	/license subdirectory.\n" + 
        		"\n" + 
        		"By installing and using this software, you accept the terms and conditions of the RealObjects PDFreactor Software License Agreement notices and the additional license agreements for third-party software and/or open source software.");
	
     // Add user style sheets
       
        
        
        config.setUserStyleSheets(
            new Resource().setContent("@page {" +
                                          "@top-center {" +
                                              "content: url(http://wpsites.net/wp-content/uploads/2013/09/default-header.png);" +
                                          "}" +
                                          " @bottom-center {" +
                                          "content: url(http://wpsites.net/wp-content/uploads/2013/09/default-header.png);" +
                                          "}" +
                                          "@bottom-right {"
                                          + " content: counter(page);"
                                          + "vertical-align: top;"
                                          + "font-size: 118pt;"
                                          + "color: black;}"
                                          +"margin-top: 250px;"
                                          +"margin-bottom: 250px;"+
                                          
                                          
                                      "}"),
            new Resource().setUri("common.css"));
	config.setDocument("<html>\n" + 
			"<body>\n" + 
			"\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"<p><b>AAAAAAAAAAAAAAA</b></p>\n" + 
			"<p><i>BBBBBBBBBBBBBB</i></p>\n" + 
			"<p>CCCCCCCCCC<sub> subscript</sub> and <sup>superscript</sup></p>\n" + 
			"\n" + 
			"</body>\n" + 
			"</html>");
	
	
	 try {
		Result result = pdfReactor.convert(config);
		 if (result != null) {
             byte[] pdf = result.getDocument();
             
             //Save the pdf at the desired location
             FileOutputStream fos = new FileOutputStream("test11.pdf");
             fos.write(pdf);
             fos.close();
             System.out.println("done!!!!!");
         }
		
		
	} catch (PDFreactorWebserviceException e) {
		Result result = e.getResult();
        System.err.println(result.getError());
		e.printStackTrace();
	}
	
	
	}
	
	
	
	
	
	

}
